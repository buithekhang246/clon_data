# import mariadb
# import json
# import time
# import sys
# from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
# import os
# import datetime
#
# config = {
#     'host': '192.168.1.32',
#     'port': 43306,
#     'user': 'root',
#     'password': 'haiphong2021',
#     'database': 'ris',
# }
# url_base = "http://localhost:6789"
#
# requestFile = False
# parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
# parser.add_argument("-s", "--start_date", default="all", type=str,
#                     help="Start day to scan data if not transport will default is the first day formatDate :YYYY-mm-dd")
# parser.add_argument("-e", "--end_date", default="all", type=str,
#                     help="The default end date with no data is today formatDate :YYYY-mm-dd")
# args = vars(parser.parse_args())
# start_date = args["start_date"]
# end_date = args["end_date"]
#
# cwd = os.getcwd()  # Get the current working directory (cwd)
# files = str(cwd) + "/modality.json"
# if not os.path.exists(files):
#     print("File path does not exist modality.json")
#     sys.exit()
# if not os.path.isfile(files):
#     print("File path  is not a file modality.json")
#     sys.exit()
# modality = json.load(open(files))
#
#
# sqlRis_study = "select ris_study.id,     ris_study.accessionNumber   " \
#                " as accessionNumber,ris_study.studyIUID as studyInstanceUID," \
#                "ris_study.part               as bodyPartExamined,\n       " \
#                " DATE_FORMAT(ris_study.date, '%T')       as studyTime, " \
#                "DATE_FORMAT(ris_study.date, '%Y-%m-%d') as studyDate,\n" \
#                "       ris_study.studyDescription              as description,\n" \
#                "       ris_study.modalityDicom                 as modalityType,\n" \
#                "       ris_study.numInstances                  as numOfImages,\n" \
#                "       ris_study.numSeries                     as numOfSeries,\n" \
#                "       ris_study.modalityFK                    as modalityFK,\n" \
#                '        ris_study.studyDescription              as description,' \
#                '   ris_study.modalityDicom                 as modalityType,' \
#                "       ris_study.patientFK                     as patientFK,\n" \
#                "       ris_study.mwlFK                         as mwlFK,\n" \
#                "       ris_study.patientNameSearch              as patientNameSearch\n" \
#                "   from ris_study where 1 = 1 "
# end_date_parameter = end_date
# if start_date != 'all':
#     sqlRis_study = sqlRis_study + " and ris_study.date_date >= ?"
# if end_date != 'all':
#     sqlRis_study = sqlRis_study + " and ris_study.date_date <= ?"
# else:
#     end_date_parameter = datetime.date.today()
#     sqlRis_study = sqlRis_study + " and ris_study.date_date <= ?"
#
# results: any
# conn = mariadb.connect(**config)
# cur = conn.cursor()
# if start_date == 'all' and end_date == 'all':
#         cur.execute(sqlRis_study, (end_date_parameter,))
#         results = cur.fetchall()
# else:
#     if start_date == 'all':
#         cur.execute(sqlRis_study, (end_date_parameter,))
#     if start_date != 'all':
#         cur.execute(sqlRis_study, (start_date, end_date_parameter,))
#     results = cur.fetchall()
#
# start_time = time.time()
# print("--- %s seconds ---" % (time.time() - start_time))
# i = 0
# total = 0
# json_data_list = []
# for row in results:
#     jsonData = {}
#     if row[1] is None:
#         jsonData["accessionNumber"] = ''
#     else:
#         jsonData["accessionNumber"] = row[1]
#     jsonData["studyInstanceUID"] = row[2]
#     jsonData["studyTime"] = row[4]
#     jsonData["studyDate"] = row[5]
#     jsonData["bodyPartExamined"] = row[3]
#     jsonData['referringPhysician'] = None
#     if row[10] is not None:
#         jsonData["aeTitle"] = modality[str(row[10])]["aeTitle"]
#         jsonData["ipAddress"] = modality[str(row[10])]["ipAddress"]
#         jsonData["institutionName"] = modality[str(row[10])]["institutionName"]
#         jsonData["manufacturerModelName"] = modality[str(row[10])]["manufacturerModelName"]
#         jsonData["stationName"] = modality[str(row[10])]["stationName"]
#     jsonData["description"] = row[11]
#     jsonData["modalityType"] = row[12]
#     jsonData["priority"] = None
#     jsonData["operatorName"] = None
#     jsonData['gender'] = None
#     jsonData['birthDate'] = None
#     jsonData['patientId'] = None
#     jsonData['patientName'] = None
#     jsonData["numOfImages"] = row[8]
#     jsonData["numOfSeries"] = row[9]
#     query = "select ris_mwl.emergency,ris_mwl.jsonData from ris_mwl where ris_mwl.id =%s"
#     cur.execute(query, (row[14],))
#     resultsMwl = cur.fetchall()
#     if len(resultsMwl) != 0:
#         for row_mwl in resultsMwl:
#             if row_mwl[1] is not None:
#                 data_x = json.loads(row_mwl[1])
#                 try:
#                     jsonData['referringPhysician'] = data_x["clinical"]["doctor"]
#                 except:
#                     jsonData['referringPhysician'] = None
#             if row_mwl[0] == 0:
#                 jsonData['priority'] = "Cap cuu"
#             else:
#                 jsonData['priority'] = "Khong cap cu"
#     query_patient = "select ris_patient.pid,ris_patient.birthYear,ris_patient.name,ris_patient.sex from " \
#                     "ris_patient where ris_patient.id =%s "
#     cur.execute(query_patient, (row[13],))
#     resultsPatient = cur.fetchall()
#     #
#     if len(resultsPatient) != 0:
#         for row_patient in resultsPatient:
#             print(row_patient)
#             jsonData['patientId'] = row_patient[0]
#             jsonData['birthDate'] = row_patient[1]
#             if row_patient[2] is None:
#                 jsonData['patientName'] = row[15]
#             else:
#                 jsonData['patientName'] = row_patient[2]
#             jsonData['gender'] = row_patient[3]
#     else:
#         break
#     json_data_list.append(jsonData)
#
# cwd = os.getcwd()
# files = str(cwd) + "/study/data_study.json"
# writeFile = open(files, 'w+', encoding='utf-8')
# writeFile.write(json.dumps(json_data_list))
#
# #     url = url_base + "/conn/ws/rest/v1/hospital/31313/study"
# #     x = requests.post(url, data=json.dumps(jsonData),
# #                       headers={"Authorization": "Basic Y29ubjMxMzEzOmlUZWNoMTIzNA==",
# #                                "Content-Type": "application/json"})
# #     print(x.content)
# #     total = total + 1
# #     response_api = json.loads(x.content)
# #     if x.status_code != 200 and response_api['header']['code'] != 200:
# #         jsonError.append(jsonData)
# #         print(response_api['header']['code'])
# #         print(jsonError)
# #         print("total error " + str(i) + "/" + str(total))
# #         cwd = os.getcwd()  # Get the current working directory (cwd)
# #         files = str(cwd) + "/error_study.txt"
# #         if not os.path.exists(files) and not os.path.isfile(files):
# #             print("File path does not exist.")
# #         else:
# #             with open(files, 'w') as outFile:
# #                 outFile.write(json.dumps(jsonError))
# # print("--- %s seconds2 ---" % (time.time() - start_time))
#
#
# # def row_file(row):
# #     return call_api(row)
# #
# #
# # def call_api(json):
# #     url = url_base + "/conn/ws/rest/v1/hospital/31313/study"
# #     x = requests.post(url, data=json.dumps(jsonData),
# #                       headers={"Authorization": "Basic Y29ubjMxMzEzOmlUZWNoMTIzNA==",
# #                                "Content-Type": "application/json"})


import sys
import os
import json
list = [('OP', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('SC', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('ED', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('XC', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('PX', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('SR', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('IO', '2022-02-26 15:42:36.000', 'sysadmin'),
        ('PR', '2022-02-26 15:42:36.000', 'sysadmin')]

s="{'orderNumber': '21.0203.017001', 'modalityType': 'CT', 'accessionNumber': '21.0203.017001', 'clinicalDiagnosis': None, 'requestedDepartmentCode': '', 'referringPhysicianCode': '', 'services': [{'requestedNumber': '123.255562890759449.1720751893256601', 'requestedProcedureCode': 'CT7733', 'requestedProcedureName': 'Chụp cắt lớp vi tính ngực liều xạ thấp sàng lọc Ung thư phổi [Chụp trên máy 128 dãy]', 'requestedModalityType': 'CT', 'requestedDatetime': '2021-10-05 09:27:40'}], 'requestedDepartmentName': 'Phòng Chăm Sóc Sức Khỏe Cộng Đồng', 'referringPhysicianName': 'BSCKI. Lê Thị Thanh', 'instructions': '', 'orderDatetime': '2021-10-05 09:27:40', 'patient': {'address': None, 'pid': '21130133', 'birthDate': '19920101', 'fullname': 'NGUYỄN THU THỦY', 'gender': 'F', 'Phone': None}}"
print(str(json.dumps(s)))
