import mariadb
import json
import time
import os
import numpy as np
from piny import YamlLoader, StrictMatcher

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}

urlBase = config_yml['url_base']
modality = {
    '1': 'CT',
    '2': 'MR',
    '11': 'DX',
    '9': 'US',
    '17': 'ES',
    '19': 'OP',
    '18': 'SC',
    '16': 'ECG',
    '10': 'ED',
    '12': 'XA'
}
conn = mariadb.connect(**config)
cur = conn.cursor()
start_time = time.time()

countAPI = 0
countGetDB = 0

start_time_time = time.time()

sqlRis = "select\n" \
         "    fm.accessionNumber,\n" \
         "    fm.groupModality,\n" \
         "    fm.diagnosis,\n" \
         "    fm.jsonData,\n" \
         "    fm.studyIUID,\n" \
         "  fm.patientFK ,fm.id\n" \
         "from ris_mwl fm where 1=1 \n"

cur.execute(sqlRis)
results = cur.fetchall()
print("--- %s seconds ---" % (time.time() - start_time_time))
json_success = []
json_error = []

query_patient = "select rp.id,rp.address,rp.pid,rp.birthYear,rp.name,rp.sex,rp.phone from ris_patient rp"

cur.execute(query_patient)
results_patient = cur.fetchall()
jsonMapPatient = {}
for row in results_patient:
    jsonMapPatient[str(row[0])] = row

for row in results:
    jsonData = {}
    isCallApi = 1
    jsonData['orderNumber'] = row[0]
    jsonData['modalityType'] = modality.get(row[1])
    jsonData['accessionNumber'] = row[0]
    jsonData['clinicalDiagnosis'] = row[2]
    if row[3] is not None:
        dataJson = json.loads(row[3])
        jsonData['requestedDepartmentCode'] = ''
        jsonData['referringPhysicianCode'] = ''
        dateListOrderProcedure = []
        if dataJson.get("orders") is not None and len(dataJson.get("orders")):
            orderProcedure = {}
            orderProcedure['requestedNumber'] = row[4]
            orderProcedure['requestedProcedureCode'] = dataJson['orders'][0]['code']
            orderProcedure['requestedProcedureName'] = dataJson['orders'][0]['name']
            orderProcedure['requestedModalityType'] = dataJson['orders'][0]['group']
            orderProcedure['requestedDatetime'] = dataJson['orderDate']
            dateListOrderProcedure.append(orderProcedure)
        else:
            isCallApi = 0
        jsonData['services'] = dateListOrderProcedure
        if jsonData['services'] is None and len(jsonData['services']) == 0:
            isCallApi = 0

        try:
            jsonData['requestedDepartmentName'] = dataJson['clinical']['department']
            jsonData['referringPhysicianName'] = dataJson['clinical']['doctor']
            jsonData['instructions'] = dataJson['comment']
            jsonData['orderDatetime'] = dataJson['orderDate']
        except:
            print("data convert error")
            isCallApi = 0

        row_patient = jsonMapPatient[str(row[5])]
        jsonPatientDTO = {}
        jsonPatientDTO['address'] = row_patient[1]
        jsonPatientDTO['pid'] = row_patient[2]
        if row_patient[3] is None:
            jsonPatientDTO['birthDate'] = None
        else:
            jsonPatientDTO['birthDate'] = str(row_patient[3]) + "0101"
        jsonPatientDTO['fullname'] = row_patient[4]
        jsonPatientDTO['gender'] = row_patient[5]
        jsonPatientDTO['Phone'] = row_patient[6]
        jsonData['patient'] = jsonPatientDTO
    else:
        isCallApi = 0

    if isCallApi == 1:

        json_success.append(jsonData)
    else:
        jsonData['id'] = row[6]
        json_error.append(jsonData)
cwd = os.getcwd()  # Get the current working directory (cwd)
data_json_slip = np.array_split(json_success, 3)
idex = 0
for index_data in data_json_slip:
    files_success = str(cwd) + "/order/data_order_success_" + str(idex) + "_.json"
    logOrderError = open(files_success, "w+", encoding="utf-8")
    logOrderError.write(json.dumps(json.dumps(index_data.tolist())))
    idex=idex+1
files_error = str(cwd) + "/data_order_error.json"
logOrderError = open(files_error, "w+", encoding="utf-8")
logOrderError.write(json.dumps(json_error))

print("--- %s seconds ---" % (time.time() - start_time_time))

print("done")
