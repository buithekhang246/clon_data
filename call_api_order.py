import requests
import os
import sys
import json
import time
import threading
from piny import YamlLoader, StrictMatcher
import requests
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--file_nane", default="all", type=str,
                    help="file name call api")

parser.add_argument("-u", "--url", default="all", type=str,
                    help="url call api")

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

file_name = "/order/data_order_success0.json"
url_base = config_yml['url_base']

url_array = {'1': 'http://192.168.1.13:6789', '0': 'http://localhost:6789', '2': 'http://192.168.1.13:6789'}

args = vars(parser.parse_args())
fila_name_args = args["file_nane"]
url_arg = args["url"]
if fila_name_args != 'all':
    file_name = "/order/data_order_success" + fila_name_args + ".json"
if url_arg != 'all':
    url_base = url_array[url_arg]

print(file_name)
print(url_base)
basic = config_yml['basic']
files = str(cwd) + file_name

start_time = time.time()
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()

data_call_api = json.load(open(files))
index_file = len(data_call_api)
print("Reading json file time %s" % (time.time() - start_time))

print(index_file)


def call_api(index_start, index_end, thread_index):
    jsonError = []
    total = 0
    i_z = 0
    if index_start > index_file:
        return
    print(index_start, index_end, index_file)
    for i in range(index_start, index_end):
        url = url_base + "/conn/ws/rest/v1/hospital/31313/order"
        row = data_call_api[i]
        s1 = time.time()
        # try:
        time.sleep(0.05)

        x = requests.post(url, data=json.dumps(row),
                          headers={"Authorization": "Basic " + basic,
                                   "Content-Type": "application/json"})
        print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
        total = total + 1
        print("total %s thread %s" % (str(total), str(thread_index)))
        if x.status_code != 200:
            print(row)
            print(x.content)

            i_z = i_z + 1
            jsonError.append(row)
            print("total error " + str(i_z) + "/" + str(total))
            cwd = os.getcwd()  # Get the current working directory (cwd)
            files = str(cwd) + "/order/error_order" + str(thread_index) + ".txt"
            print(files)
            # break
            os.makedirs(os.path.dirname(files), exist_ok=True)
            with open(files, 'w+') as outFile:
                outFile.write(json.dumps(jsonError))
    # except:
    #     print("aaaa")


print("--- %s seconds2 end thread---" % (time.time() - start_time))

print("--- %s end end thread---" % (time.time() - start_time))

try:
    threads = []
    # imdex = 0
    # index_thread = 50000
    # while True:
    #     if ((imdex + 1) * index_thread) > index_file:
    #         t1 = threading.Thread(target=call_api, args=(index_thread * imdex, index_file, imdex))
    #         threads.append(t1)
    #         t1.start()
    #         break
    #     t1 = threading.Thread(target=call_api,
    #                           args=(index_thread * imdex, index_thread + index_thread * imdex - 1, imdex))
    #     t1.start()
    #     threads.append(t1)
    #     imdex = imdex + 1
    # for thread in threads:
    #     thread.join()

    thread = 20
    index_row = int(index_file / thread)
    for imdex in range(thread):
        if imdex == thread - 1:
            t1 = threading.Thread(target=call_api, args=((thread - 1) * index_row, index_file, imdex))
            t1.start()
            threads.append(t1)
        else:
            t1 = threading.Thread(target=call_api, args=(imdex * index_row, index_row * (imdex + 1) - 1, imdex))
            t1.start()
            threads.append(t1)
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
