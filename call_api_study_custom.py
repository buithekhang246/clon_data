import requests
import os
import sys
import json
import time
import threading
import mariadb
from piny import YamlLoader, StrictMatcher
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

cwd = os.getcwd()  # Get the current working directory (cwd)
config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--file_nane", default="all", type=str,
                    help="file name call api")

parser.add_argument("-u", "--url", default="all", type=str,
                    help="url call api")


url_base = config_yml['url_base']
basic = config_yml['basic']
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}
sql_accnumber = "SELECT DISTINCT  ho.order_number  FROM hosp_order ho  "
conn2 = mariadb.connect(**itechConfig)
cur2 = conn2.cursor()
cur2.execute(sql_accnumber)
results = cur2.fetchall()
list_accmumer = {}
for number in results:
    list_accmumer[number[0]] = 1
files = str(cwd) + "/study/study_success.json"

args = vars(parser.parse_args())
fila_name_args = args["file_nane"]
url_arg = args["url"]
if fila_name_args != 'all':
    files = "/study/study_success" + fila_name_args + ".json"


len(results)
start_time = time.time()
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()

data_call_api = json.load(open(files))
index_file = len(data_call_api)
print("Reading json file time %s" % (time.time() - start_time))
print(index_file)

print(url_base)


def call_api(index_start, index_end, thread_index):
    json_error = []
    total = 0
    i_z = 0
    for i in range(index_start, index_end):
        url = url_base + "/conn/ws/rest/v1/hospital/31313/study"
        row = data_call_api[i]
        s1 = time.time()

        if list_accmumer.get(row['accessionNumber']) is not None:
            row["studyTime"] = str(row["studyTime"]).replace(':', '')
            row["studyDate"] = str(row["studyDate"]).replace('-', '')
            x = requests.post(url, data=json.dumps(row),
                              headers={"Authorization": "Basic " + basic,
                                       "Content-Type": "application/json"})
            print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
            total = total + 1
            if x.status_code != 200 or json.loads(x.content)['header']['code'] != 200:
                # print(row)
                i_z = i_z + 1
                json_error.append(row)
                cwd = os.getcwd()  # Get the current working directory (cwd)
                files = str(cwd) + "/study/error_study" + str(thread_index) + ".txt"
                # print(files)
                # break
                os.makedirs(os.path.dirname(files), exist_ok=True)
                # print(files)
                with open(files, 'w+') as outFile:
                    outFile.write(json.dumps(json_error))
            print("total error " + str(i_z) + "/" + str(total) + "thread" + str(thread_index))

    print("--- %s seconds2 end thread---" % (time.time() - start_time))


print("--- %s end end thread---" % (time.time() - start_time))
try:
    threads = []


    thread = 20
    index_row = int(index_file / thread)
    for imdex in range(thread):
        if imdex == thread - 1:
            t1 = threading.Thread(target=call_api, args=((thread - 1) * index_row, index_file, imdex))
            t1.start()
            threads.append(t1)
        else:
            t1 = threading.Thread(target=call_api, args=(imdex * index_row, index_row * (imdex + 1) - 1, imdex))
            t1.start()
            threads.append(t1)
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
