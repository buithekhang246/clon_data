import sys

import mariadb
import json
import time
import requests
import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from piny import YamlLoader, StrictMatcher

import threading

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
url_base = config_yml['url_base']

config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}


def get_patient(jsonData, row_data, row_db):
    if row_data is not None:
        jsonData['patientId'] = row_data[0]
        jsonData['birthDate'] = row_data[1]
        if row_data[2] is None:
            jsonData['patientName'] = row_db[15]
        else:
            jsonData['patientName'] = row_data[2]
        jsonData['gender'] = row_data[3]
    else:
        jsonData['error'] = True


def get_ris_mwl(jsonData, resultsMwl):
    if resultsMwl is not None:
        if resultsMwl[1] is not None:
            data_x = json.loads(resultsMwl[1])
            try:
                jsonData['referringPhysician'] = data_x["clinical"]["doctor"]
            except:
                jsonData['referringPhysician'] = None
        if resultsMwl[0] == 0:
            jsonData['priority'] = "Cap cuu"
        else:
            jsonData['priority'] = "Khong cap cu"


def convent_row_data(jsonData, row):
    if row[1] is None:
        jsonData["accessionNumber"] = ''
    else:
        jsonData["accessionNumber"] = row[1]
    jsonData["studyInstanceUID"] = row[2]
    jsonData["studyTime"] = str(row[4]).replace(':', '')
    jsonData["studyDate"] = row[5]
    jsonData["bodyPartExamined"] = row[3]
    jsonData['referringPhysician'] = None
    if row[10] is not None:
        jsonData["aeTitle"] = modality[str(row[10])]["aeTitle"]
        jsonData["ipAddress"] = modality[str(row[10])]["ipAddress"]
        jsonData["institutionName"] = modality[str(row[10])]["institutionName"]
        jsonData["manufacturerModelName"] = modality[str(row[10])]["manufacturerModelName"]
        jsonData["stationName"] = modality[str(row[10])]["stationName"]
    jsonData["description"] = row[11]
    jsonData["modalityType"] = dataGroup.get(str(row[10]))['group']
    jsonData["priority"] = None
    jsonData["operatorName"] = None
    jsonData['gender'] = None
    jsonData['birthDate'] = None
    jsonData['patientId'] = None
    jsonData['patientName'] = None
    jsonData["numOfImages"] = row[8]
    jsonData["numOfSeries"] = row[9]
    if row[14] is not None:
        data = data_ris_mwl[str(row[14])]
        get_ris_mwl(jsonData=jsonData, resultsMwl=data)
    if row[13] is not None:
        data = data_ris_patient[str(row[13])]
        get_patient(jsonData=jsonData, row_data=data, row_db=row)
    else:
        jsonData['error'] = True


cwd = os.getcwd()
files = str(cwd) + "/modality.json"
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()
modality = json.load(open(files))

conn = mariadb.connect(**config)
cur = conn.cursor()
start_time = time.time()
sqlRis_study = "select ris_study.id,     ris_study.accessionNumber   " \
               " as accessionNumber,ris_study.studyIUID as studyInstanceUID," \
               "ris_study.part               as bodyPartExamined,\n       " \
               " DATE_FORMAT(ris_study.date, '%T')       as studyTime, " \
               "DATE_FORMAT(ris_study.date, '%Y%m%d') as studyDate,\n" \
               "       ris_study.studyDescription              as description,\n" \
               "       ris_study.modalityDicom                 as modalityType,\n" \
               "       ris_study.numInstances                  as numOfImages,\n" \
               "       ris_study.numSeries                     as numOfSeries,\n" \
               "       ris_study.modalityFK                    as modalityFK,\n" \
               '        ris_study.studyDescription              as description,' \
               '   ris_study.modalityDicom                 as modalityType,' \
               "       ris_study.patientFK                     as patientFK,\n" \
               "       ris_study.mwlFK                         as mwlFK,\n" \
               "       ris_study.patientNameSearch              as patientNameSearch\n" \
               "   from ris_study where 1 = 1  and ris_study.date_date > ? and ris_study.date_date < ?"

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--start_date", default="all", type=str,
                    help="Start day to scan data if not transport will default is the first day formatDate :YYYY-mm-dd")
parser.add_argument("-e", "--end_date", default="all", type=str,
                    help="The default end date with no data is today formatDate :YYYY-mm-dd")
args = vars(parser.parse_args())

start_date = args["start_date"]
end_date = args["end_date"]

if start_date > end_date:
    print("end_date lon hon start_date")
    sys.exit()
if start_date == "all" or end_date == 'all':
    print("Nhap day du start date vs end date")
    sys.exit()

s1 = time.time()
sql_mwl = "select ris_mwl.emergency,ris_mwl.jsonData,ris_mwl.id from ris_mwl "
cur.execute(sql_mwl)
data_result_mwl = cur.fetchall()
data_ris_mwl = {}
for row in data_result_mwl:
    data_ris_mwl[str(row[2])] = row
print("--- %s seconds ---" % (time.time() - s1))
s1 = time.time()
sql_patient = "select ris_patient.pid,ris_patient.birthYear,ris_patient.name,ris_patient.sex,ris_patient.id from " \
              "ris_patient  "
cur.execute(sql_patient)
data_result_patient = cur.fetchall()
data_ris_patient = {}
for row in data_result_patient:
    data_ris_patient[str(row[4])] = row
print("--- %s seconds ---" % (time.time() - s1))
start_time = time.time()
cur.execute(sqlRis_study, (start_date, end_date,))
results = cur.fetchall()
print(len(results))
print("--- %s seconds ---" % (time.time() - start_time))

json_data = []
json_data_error = []
for row in results:
    jsonData = {}
    convent_row_data(jsonData, row)
    if jsonData.get('error') is None:
        json_data.append(jsonData)
    else:
        json_data_error.append(jsonData)
files_error = str(cwd) + "/study/study_error_start_date.json"
os.makedirs(os.path.dirname(files), exist_ok=True)
with open(files_error, 'w+') as outFileError:
    outFileError.write(json.dumps(json_data_error))

index_file = len(json_data)

sql_conn_group = 'select rim.id,rim.modalityCode , rmg.modality from ris_modality as rim left join ris_modality_group ' \
                 'rmg  ' \
                 'on rim.`group` = rmg.id '
cur.execute(sql_conn_group)
results_1 = cur.fetchall()
dataGroup = {}
for row_data in results_1:
    group = {}
    group['id'] = row_data[0]
    group["group"] = row_data[1]
    if row_data[0] == 96:
        group["group"] = "DX"
    if row_data[1] == '-1':
        if row_data[0] == 148:
            group["group"] = "DX"
        else:
            group["group"] = row_data[2]
    dataGroup[str(row_data[0])] = group


def call_api(index_start, index_end, thread_index):
    json_error = []
    total = 0
    i_z = 0
    if index_start > index_file:
        return
    print(index_start, index_end, thread_index)
    for i in range(index_start, index_end):
        url = url_base + "/conn/ws/rest/v1/hospital/31313/study"
        row = json_data[i]
        s1 = time.time()
        x = requests.post(url, data=json.dumps(row),
                          headers={"Authorization": "Basic Y29ubjMxMzEzOmlUZWNoMTIzNA==",
                                   "Content-Type": "application/json"})
        print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
        # print(x.content)
        total = total + 1
        if x.status_code != 200:
            print(row)
            i_z = i_z + 1
            json_error.append(row)
            print("total error " + str(i_z) + "/" + str(total))
            files_er = str(cwd) + "/study/end_start/error_study_start_end" + str(thread_index) + ".txt"
            print(files_er)
            # break
            os.makedirs(os.path.dirname(files_er), exist_ok=True)

            print(files_er)
            with open(files_er, 'w+') as outFile:
                outFile.write(json.dumps(json_error))
    print("--- %s seconds2 end thread---" % (time.time() - start_time))


print("--- %s end end thread---" % (time.time() - start_time))

try:
    threads = []
    imdex = 0
    index_thread = 100000
    while True:
        if ((imdex + 1) * index_thread) > index_file:
            t1 = threading.Thread(target=call_api, args=(index_thread * imdex, index_file, imdex))
            threads.append(t1)
            t1.start()
            break
        t1 = threading.Thread(target=call_api,
                              args=(index_thread * imdex, index_thread + index_thread * imdex - 1, imdex))
        t1.start()
        threads.append(t1)
        imdex = imdex + 1
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
