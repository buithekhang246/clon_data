import requests
import os
import sys
import json
import time
import threading
import mariadb
from piny import YamlLoader, StrictMatcher
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import datetime

cwd = os.getcwd()  # Get the current working directory (cwd)
config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--file_nane", default="all", type=str,
                    help="file name call api")

parser.add_argument("-u", "--url", default="all", type=str,
                    help="url call api")


url_base = config_yml['url_base']
basic = config_yml['basic']
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

files = str(cwd) + "/order/data_order_success.json"

sql_accnumber = "SELECT DISTINCT  ho.order_number  FROM hosp_order ho  "
conn2 = mariadb.connect(**itechConfig)
cur2 = conn2.cursor()
cur2.execute(sql_accnumber)
results = cur2.fetchall()
list_accmumer = {}
for number in results:
    list_accmumer[number[0]] = 1

len(list_accmumer)
basic = config_yml['basic']

start_time = time.time()
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()

data_call_api = json.load(open(files))
index_file = len(data_call_api)
print("Reading json file time %s" % (time.time() - start_time))

print(index_file)


def call_api(index_start, index_end, thread_index):
    jsonError = []
    total = 0
    i_z = 0
    if index_start > index_file:
        return
    print(index_start, index_end, index_file)
    url = url_base + "/conn/ws/rest/v1/hospital/31313/order"
    for i in range(index_start, index_end):
        row = data_call_api[i]
        if list_accmumer.get(row['orderNumber']) is None:
            s1 = time.time()
            # try:
            time.sleep(0.05)
            print(row['orderNumber'])

            date1 = datetime.datetime.strptime(row['orderDatetime'], '%Y-%m-%d %H:%M:%S')
            row['orderDatetime'] = datetime.datetime.strftime(date1, '%Y%m%d%H%M%S')
            row['services'][0]['requestedDatetime'] = datetime.datetime.strftime(date1, '%Y%m%d%H%M%S')

            x = requests.post(url, data=json.dumps(row),
                              headers={"Authorization": "Basic " + basic,
                                       "Content-Type": "application/json"})
            total = total + 1
            if x.status_code != 200:
                print(row)
                print(x.content)

                i_z = i_z + 1
                jsonError.append(row)
                print("total error " + str(i_z) + "/" + str(total))
                cwd = os.getcwd()  # Get the current working directory (cwd)
                files = str(cwd) + "/order/error_order_error" + str(thread_index) + ".txt"
                print(files)
                # break
                os.makedirs(os.path.dirname(files), exist_ok=True)
                with open(files, 'w+') as outFile:
                    outFile.write(json.dumps(jsonError))
            print("total %s thread %s" % (str(total), str(thread_index)))
            print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))

    # except:
    #     print("aaaa")


print("--- %s seconds2 end thread---" % (time.time() - start_time))

print("--- %s end end thread---" % (time.time() - start_time))

try:
    threads = []
    # imdex = 0
    # index_thread = 50000
    # while True:
    #     if ((imdex + 1) * index_thread) > index_file:
    #         t1 = threading.Thread(target=call_api, args=(index_thread * imdex, index_file, imdex))
    #         threads.append(t1)
    #         t1.start()
    #         break
    #     t1 = threading.Thread(target=call_api,
    #                           args=(index_thread * imdex, index_thread + index_thread * imdex - 1, imdex))
    #     t1.start()
    #     threads.append(t1)
    #     imdex = imdex + 1
    # for thread in threads:
    #     thread.join()

    thread = 20
    index_row = int(index_file / thread)
    for imdex in range(thread):
        if imdex == thread - 1:
            t1 = threading.Thread(target=call_api, args=((thread - 1) * index_row, index_file, imdex))
            t1.start()
            threads.append(t1)
        else:
            t1 = threading.Thread(target=call_api, args=(imdex * index_row, index_row * (imdex + 1) - 1, imdex))
            t1.start()
            threads.append(t1)
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
