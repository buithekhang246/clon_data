import json
import time
import unicodedata
import os
import mariadb
import requests
from piny import YamlLoader, StrictMatcher

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

conn = mariadb.connect(**config)
cur = conn.cursor()

start_time = time.time()
user_pass = {
    "password": config_yml['user_pass']['pass'], "username": config_yml['user_pass']['user']
}
base_url = config_yml['url_base']
# base_url = 'http://localhost:6789'
tokenReuest = requests.post(base_url + '/ws/rest/v1/hospital/31313/login',
                            data=json.dumps(user_pass),
                            headers={
                                "accept": "*",
                                'Content-Type': 'application/json'
                            })
token = json.loads(tokenReuest.content)['token']['access_token']
# print(token)
request_procedure = requests.get(base_url + '/secured/ws/rest/v1/async/hospital/31313/procedure?limit=10000',
                                 headers={
                                     "accept": "*",
                                     'Content-Type': 'application/json',
                                     "Authorization": 'Bearer ' + str(token)
                                 })
dataBody = json.loads(request_procedure.content)['body']
jsonHost = {}
for row in dataBody:

    jsonHost[row['code']] = row

sql = 'select rsc.`key`,rsc.val,rmg.name  from ris_service_code rsc left join ris_modality_group rmg on ' \
      "rsc.modalityGroupFK=rmg.id "
cur.execute(sql)
rest_procedure = cur.fetchall()
i = 0
lendata = []
for row_data in rest_procedure:
    # if jsonHost.get(str(row_data[0]).strip()) is None:
    data = {}
    data['code'] = row_data[0]
    data['name'] = row_data[1]
    if row_data[2] != 'Mammo':
        data['modalityType'] = row_data[2]
    else:
        data['modalityType'] = 'MG'
    decom_description = ''.join(
        (c for c in unicodedata.normalize('NFD', row_data[1]) if unicodedata.category(c) != 'Mn'))
    data['dicomDescription'] = decom_description
    data['suggestEnabled'] = False
    data['bodyParts'] = []
    if len(row_data[1]) < 6:
        data['name'] = data['name'] + "   "
    # print(json.dumps(data))
    lendata.append(data)
    # request_x = requests.post(
    #     base_url + '/secured/ws/rest/v1/async/hospital/31313/procedure',
    #     headers={
    #         "accept": "*",
    #         'Content-Type': 'application/json',
    #         "Authorization": 'Bearer ' + str(token)
    #     }, data=json.dumps(data))

print(json.dumps(lendata))

# api get sql


# jsonHost
print(len(lendata))
print(len(dataBody))



data_1=[]
for row in lendata:
    # print("cai cu")
    # print(jsonHost[row['code']])
    jsonHost[row['code']]['name'] = row['name']
    # data_1.append(jsonHost[row['code']])

    # print("cai moi")
    # print(jsonHost[row['code']])
    request_x = requests.put(
        base_url + '/secured/ws/rest/v1/async/hospital/31313/procedure',
        headers={
            "accept": "*",
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + str(token)
        }, data=json.dumps(jsonHost[row['code']]))

    print(request_x.content)

#
cwd = os.getcwd()  # Get the current working directory (cwd)
files = str(cwd) + "/filelog/procedure_2.json"
print(files)
os.makedirs(os.path.dirname(files), exist_ok=True)
with open(files, 'w+') as outFile:
    outFile.write(json.dumps(data_1))
