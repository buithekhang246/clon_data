import mariadb
import json
import time
import requests
import os
from piny import YamlLoader, StrictMatcher


cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}
urlBase = config_yml['url_base']
# urlBase = "http://localhost:6787"
# get data in db vg
sqlRis_mwl = "select cu.fullName , cu.account , cu.pass  from cores_user cu ;"
conn = mariadb.connect(**config)
cur = conn.cursor()
start_time = time.time()
cur.execute(sqlRis_mwl)
results = cur.fetchall()
print("--- %s seconds ---" % (time.time() - start_time))
print("count record: ", len(results))
# get token
bodyData = {
    "password": config_yml['user_pass']['pass'], "username": config_yml['user_pass']['user']
}
urlToken = urlBase + "/ws/rest/v1/hospital/31313/login"
x = requests.post(urlToken, data=json.dumps(bodyData),
                  headers={
                      "Content-Type": "application/json;charset=UTF-8"})
print(x.content)
token = x.json()["token"]["access_token"]
# call api create user in itech
print(token)
listNameUser = []
list_update_user = []
for row in results:
    jsonData = {}
    listNameUser.append(row[1])
    jsonData['code'] = row[1]
    jsonData['fullname'] = row[0]
    jsonData['username'] = row[1]
    jsonData['password'] = 123456
    jsonData['departmentID'] = 1
    jsonData['type'] = 'IMAGING_DOCTOR'
    jsonData['groupIDs'] = [6]
    url = urlBase + "/secured/ws/rest/v1/async/hospital/31313/user"
    x = requests.post(url, data=json.dumps(jsonData),
                      headers={
                          "Authorization": "Bearer " + token,
                          "Content-Type": "application/json;charset=UTF-8"})
    print(url)

    if x.content != 200:
        print(x.content)
        list_update_user.append(row[1])
    print(json.dumps(jsonData))

    # print(x.content)
print(list_update_user)
# update password and md5 in user itech
conn2 = mariadb.connect(**itechConfig)
cur2 = conn2.cursor()
start_time2 = time.time()

for row in results:
    sql = "update hosp_user set password = 'e10adc3949ba59abbe56e057f20f883e', password_algorithm = 'md5' where code " \
          "= '{{codeUser}}'; "
    sql = sql.replace("{{codeUser}}", row[1])
    try:
        cur2.execute(sql)
        conn2.commit()
    except:
        print("user no")

print("--- %s seconds ---" % (time.time() - start_time2) + "success")


sql_app = "INSERT INTO itpacs_ris.hosp_apps(type_id, name, vendor, description, url, config_json, enabled, hospital_id, created_time, creator_id, last_updated_time, last_updated_id) " \
          'VALUES' \
          "('his', 'HIS Connector', 'ITECH', 'HIS Connector', 'http://192.168.1.32:9876/it/rest', '{{json_user1}}', 1, '31313', '2022-05-07 10:03:15.000', 1, '2022-05-07 10:03:15.000', 1)," \
          "('portal', 'Patient portal', 'ITECH', 'Patient portal', 'http://192.168.1.32:4321/admin/ws/rest/v1/async', '{{json_user2}}', 1, '31313', '2022-05-07 10:03:15.000', 1, '2022-05-07 10:03:15.000', 1)"
user1 = {"username": "admin", "password": "admin"}
user2 = {"username": "admin", "password": "admin@1324"}

sql_app = sql_app.replace("{{json_user1}}", json.dumps(user1))
sql_app = sql_app.replace('{{json_user2}}', json.dumps(user2))

try:
    cur2.execute(sql_app)
    conn2.commit()
except:
    print("user no")



sql_role="update syst_role set type_id = 'system' where id = 'hospital-admin'"

try:
    cur2.execute(sql_role)
    conn2.commit()
except:
    print("user no")