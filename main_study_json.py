import json
import os
import sys
import time
from piny import YamlLoader, StrictMatcher
import mariadb
import numpy as nps

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}
url_base = config_yml['url_base']

print(config)
print(url_base)


def get_patient(jsonData, row_data, row_db):
    if row_data is not None:
        jsonData['patientId'] = row_data[0]
        jsonData['birthDate'] = row_data[1]
        if row_data[2] is None:
            jsonData['patientName'] = row_db[15]
        else:
            jsonData['patientName'] = row_data[2]
        jsonData['gender'] = row_data[3]
    else:
        jsonData['error'] = True


def get_ris_mwl(jsonData, resultsMwl):
    if resultsMwl is not None:
        if resultsMwl[1] is not None:
            data_x = json.loads(resultsMwl[1])
            try:
                jsonData['referringPhysician'] = data_x["clinical"]["doctor"]
            except:
                jsonData['referringPhysician'] = None
        if resultsMwl[0] == 0:
            jsonData['priority'] = "Cap cuu"
        else:
            jsonData['priority'] = "Khong cap cu"


def convent_row_data(jsonData, row):
    if row[1] is None:
        jsonData["accessionNumber"] = ''
    else:
        jsonData["accessionNumber"] = row[1]
    jsonData["studyInstanceUID"] = row[2]
    jsonData["studyTime"] = str(row[4]).replace(':', '')
    jsonData["studyDate"] = row[5]
    jsonData["bodyPartExamined"] = row[3]
    jsonData['referringPhysician'] = None
    if row[10] is not None:
        jsonData["aeTitle"] = modality[str(row[10])]["aeTitle"]
        jsonData["ipAddress"] = modality[str(row[10])]["ipAddress"]
        jsonData["institutionName"] = modality[str(row[10])]["institutionName"]
        jsonData["manufacturerModelName"] = modality[str(row[10])]["manufacturerModelName"]
        jsonData["stationName"] = modality[str(row[10])]["stationName"]
    jsonData["description"] = row[11]
    jsonData["modalityType"] = dataGroup.get(str(row[10]))['group']
    jsonData["priority"] = None
    jsonData["operatorName"] = None
    jsonData['gender'] = None
    jsonData['birthDate'] = None
    jsonData['patientId'] = None
    jsonData['patientName'] = None
    jsonData["numOfImages"] = row[8]
    jsonData["numOfSeries"] = row[9]
    # if row[14] is not None:
    #     data = data_ris_mwl[str(row[14])]
    #     get_ris_mwl(jsonData=jsonData, resultsMwl=data)
    # if row[13] is not None:
    #     data = data_ris_patient[str(row[13])]
    #     get_patient(jsonData=jsonData, row_data=data, row_db=row)
    # else:
    #     jsonData['error'] = True


files = str(cwd) + "/modality.json"
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()
modality = json.load(open(files))
sqlRis_study = "select ris_study.id,     ris_study.accessionNumber   " \
               " as accessionNumber,ris_study.studyIUID as studyInstanceUID," \
               "ris_study.part               as bodyPartExamined,\n       " \
               " DATE_FORMAT(ris_study.date, '%T')       as studyTime, " \
               "DATE_FORMAT(ris_study.date, '%Y%m%d') as studyDate,\n" \
               "       ris_study.studyDescription              as description,\n" \
               "       ris_study.modalityDicom                 as modalityType,\n" \
               "       ris_study.numInstances                  as numOfImages,\n" \
               "       ris_study.numSeries                     as numOfSeries,\n" \
               "       ris_study.modalityFK                    as modalityFK,\n" \
               '        ris_study.studyDescription              as description,' \
               '   ris_study.modalityDicom                 as modalityType,' \
               "       ris_study.patientFK                     as patientFK,\n" \
               "       ris_study.mwlFK                         as mwlFK,\n" \
               "       ris_study.patientNameSearch              as patientNameSearch\n" \
               "   from ris_study where 1 = 1 limit 100 "
results: any
conn = mariadb.connect(**config)
cur = conn.cursor()
# s1 = time.time()
# sql_mwl = "select ris_mwl.emergency,ris_mwl.jsonData,ris_mwl.id from ris_mwl "
# cur.execute(sql_mwl)
# data_result_mwl = cur.fetchall()
# data_ris_mwl = {}
# for row in data_result_mwl:
#     data_ris_mwl[str(row[2])] = row
# print("--- %s seconds ---" % (time.time() - s1))
#
# s1 = time.time()
# sql_patient = "select ris_patient.pid,ris_patient.birthYear,ris_patient.name,ris_patient.sex,ris_patient.id from " \
#               "ris_patient  "
# cur.execute(sql_patient)
# data_result_patient = cur.fetchall()
# data_ris_patient = {}
# for row in data_result_patient:
#     data_ris_patient[str(row[4])] = row
# print("--- %s seconds ---" % (time.time() - s1))
# start_time = time.time()


cur.execute(sqlRis_study)
results = cur.fetchall()

# print(len(results))
# print("--- %s seconds ---" % (time.time() - start_time))
json_success = []
json_data_error = []
i_error = 0
total = 0

sql_conn_group = 'select rim.id,rim.modalityCode , rmg.modality from ris_modality as rim left join ris_modality_group rmg  ' \
                 'on rim.`group` = rmg.id '
cur.execute(sql_conn_group)
results_1 = cur.fetchall()
dataGroup = {}
for row_data in results_1:
    group = {}
    group['id'] = row_data[0]
    group["group"] = row_data[1]
    if row_data[0] == 96:
        group["group"] = "DX"
    if row_data[1] == '-1':
        if row_data[0] == 148:
            group["group"] = "DX"
        else:
            group["group"] = row_data[2]
    dataGroup[str(row_data[0])] = group


for row in results:
    jsonData = {}
    convent_row_data(jsonData, row)
    if jsonData.get('error') is None:
        json_success.append(jsonData)
    else:
        json_data_error.append(jsonData)
data_slip_data = nps.array_split(json_success, 3)
idex = 0

# for data_json in data_slip_data:
#     print("--- %s seconds2 ---" % (time.time() - start_time))
#     # files = str(cwd) + "/study/study_success" + str(idex) + ".json"
#     # os.makedirs(os.path.dirname(files), exist_ok=True)
#     # with open(files, 'w+') as outFile:
#     #     outFile.write(json.dumps(data_json))
#     # idex = idex + 1
#     files_success = str(cwd) + "/study/study_success" + str(idex) + ".json"
#     logStudyError = open(files_success, "w+", encoding="utf-8")
#     logStudyError.write(json.dumps(data_json.tolist()))
#     idex = idex + 1
# files_error = str(cwd) + "/study/study_error.json"
# os.makedirs(os.path.dirname(files), exist_ok=True)
# with open(files_error, 'w+') as outFileError:
#     outFileError.write(json.dumps(json_data_error))
