import requests
import json
import os
import sys
import mariadb
import time
from piny import YamlLoader, StrictMatcher

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}
url_base = config_yml['url_base']

connent = mariadb.connect(**itechConfig)
cur_itech = connent.cursor()
files = str(cwd) + "/syst_modality_abbr.json"
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()
data_syst_modality_abbr = json.load(open(files))
# for i in data_syst_modality_abbr:
#     sql = "insert into syst_modality_abbr (id, created_time, creator_id) values ('{{name}}', '{{date}}'," + str(1) + ")"
#     sql = sql.replace("{{name}}", i['name'])
#     sql = sql.replace("{{date}}", i['date'])
#     print(i['creator_id'])
#     try:
#         print(sql)
#         cur_itech.execute(sql)
#         connent.commit()
#     except:
#         print("inserr error %s" % (i))

# files = str(cwd) + "/modality.json"
# if not os.path.exists(files):
#     print("File path does not exist.")
#     sys.exit()
# if not os.path.isfile(files):
#     print("File path is not a file.")
#     sys.exit()
# modality = json.load(open(files))
#
# start_time = time.time()
# user_pass = {
#     "password": config_yml['user_pass']['pass'], "username": config_yml['user_pass']['user']
# }
#
# tokenReuest = requests.post(url_base + '/ws/rest/v1/hospital/31313/login',
#                             data=json.dumps(user_pass),
#                             headers={
#                                 "accept": "*",
#                                 'Content-Type': 'application/json'
#                             })
# token = json.loads(tokenReuest.content)['token']['access_token']
#
# # tao group
#
# listGroup = ['DX', 'CT', 'MR', 'US', 'ES', 'OP', 'SC', 'ECG', 'ED', 'XA', 'MG', 'XC', 'PX', 'SR', 'OT', 'IO', 'PR',
#              'ED', 'CR']
#
# for group_row in listGroup:
#     data_group_row = {}
#     data_group_row['code'] = group_row
#     data_group_row['description'] = group_row
#
#     response_group = repones = requests.post(
#         url_base + '/secured/ws/rest/v1/async/hospital/31313/modalityGroup',
#         data=json.dumps(data_group_row),
#         headers={"Authorization": "Bearer " + token,
#                  "Content-Type": "application/json;charset=UTF-8"})
#     print(response_group.content)
#
# # tao modality
#
# listGroup = requests.get(
#     url_base + '/secured/ws/rest/v1/async/hospital/31313/modalityGroup?limit=1000&offset=0',
#     headers={"Authorization": "Bearer " + token,
#              "Content-Type": "application/json;charset=UTF-8"})
# print(token)
# data = json.loads(listGroup.content)['body']
# jsonGroup = {}
# for row in data:
#     jsonGroup[row['code']] = row
#
#
conn = mariadb.connect(**config)
cur = conn.cursor()

sql_conn_group = 'select rim.id,rim.modalityCode , rmg.modality from ris_modality as rim left join ris_modality_group rmg  ' \
           'on rim.`group` = rmg.id '
cur.execute(sql_conn_group)
results = cur.fetchall()
dataGroup = {}
for row_data in results:
    group = {}
    group['id'] = row_data[0]
    group["group"] = row_data[1]
    if row_data[0] == 96:
        group["group"] = "DX"
    if row_data[1] == '-1':
        if row_data[0] == 148:
            group["group"] = "DX"
        else:
            group["group"] = row_data[2]
    dataGroup[str(row_data[0])] = group

print(dataGroup)
# listIpAddress = {}
# i = 0
#
# sql_conn = 'select rim.id,rim.modalityCode , rmg.modality from ris_modality as rim left join ris_modality_group rmg  ' \
#            'on rim.`group` = rmg.id '
# cur.execute(sql_conn)
# results = cur.fetchall()
# dataGroup = {}
# for row_data in results:
#     group = {}
#     group['id'] = row_data[0]
#     group["group"] = row_data[1]
#     if row_data[0] == 96:
#         group["group"] = "DX"
#     if row_data[1] == '-1':
#         if row_data[0] == 148:
#             group["group"] = "DX"
#         else:
#             group["group"] = row_data[2]
#     dataGroup[str(row_data[0])] = group
#
# print(dataGroup)
# listIpAddress = {}
# i = 0
#
# for row_modality in modality.keys():
#     print(row_modality)
#     jsonBody = {}
#     jsonBody['name'] = dataGroup[str(row_modality)]['group'] + 'no' + str(i)
#     jsonBody['code'] = dataGroup[str(row_modality)]['group'] + 'no' + str(i)
#
#     jsonBody['modalityType'] = dataGroup[str(row_modality)]['group']
#     jsonBody['groupID'] = jsonGroup[dataGroup[str(row_modality)]['group']]['id']
#     jsonBody['roomNo'] = None
#     jsonBody['dicomSupported'] = True
#
#     jsonBody['institutionName'] = modality[row_modality]['institutionName']
#     jsonBody['manufacturerModelName'] = modality[row_modality]['manufacturerModelName']
#     jsonBody['stationName'] = modality[row_modality]['stationName']
#     jsonBody['aeTitle'] = modality[row_modality]['aeTitle']
#     jsonBody['ipAddress'] = modality[row_modality]['ipAddress']
#     jsonBody['enabled'] = True
#     jsonBody['false'] = False
#
#     attributes = {}
#
#     attributes['notifyIn'] = ''
#     attributes['dicomNonRequired'] = ''
#     attributes['technicianRequired'] = ''
#     jsonBody['attributes'] = attributes
#     i = i + 1
#     listIpAddress[modality[row_modality]['ipAddress']] = jsonBody
# for requestBody in listIpAddress.keys():
#     url = url_base + "/secured/ws/rest/v1/async/hospital/31313/modality"
#     response_api = requests.post(url, data=json.dumps(listIpAddress[requestBody]),
#                                  headers={"Authorization": "Bearer " + token,
#                                           "Content-Type": "application/json;charset=UTF-8"})
#     print(response_api.content)
#
# # add default
# files_modality_default = str(cwd) + "/modality_default.json"
# if not os.path.exists(files_modality_default):
#     print("File path does not exist.")
#     sys.exit()
# if not os.path.isfile(files_modality_default):
#     print("File path is not a file.")
#     sys.exit()
#
# modalitys_default = json.load(open(files_modality_default))
#
# for row_modality_default in modalitys_default:
#     row_modality_default["groupID"] = jsonGroup[row_modality_default['modalityType']]['id']
#
#     url = url_base + "/secured/ws/rest/v1/async/hospital/31313/modality"
#     respones_123 = requests.post(url, data=json.dumps(row_modality_default),
#                                  headers={"Authorization": "Bearer " + token,
#                                           "Content-Type": "application/json;charset=UTF-8"})
#     print(respones_123.content)
#
# # add modalityTypeCreate
# files_modalityTypeCreate = str(cwd) + "/modalityTypeCreate.json"
# if not os.path.exists(files_modalityTypeCreate):
#     print("File path does not exist.")
#     sys.exit()
# if not os.path.isfile(files_modalityTypeCreate):
#     print("File path is not a file.")
#     sys.exit()
# data_modalityTypeCreate = json.load(open(files_modalityTypeCreate))
#
# url = url_base + "/secured/ws/rest/v1/async/hospital/31313/modality?limit=10000"
# res_add_ = requests.get(url, headers={"Authorization": "Bearer " + token,
#                                       "Content-Type": "application/json;charset=UTF-8"})
# jsonModality = json.loads(res_add_.content)
# print(jsonModality['body'])
# jsonModalityId = {}
# for row_list in jsonModality['body']:
#     jsonModalityId[row_list['modalityType']] = row_list
#
# print(jsonModalityId)
#
# for data in data_modalityTypeCreate:
#     data['preferredModalityID'] = jsonModalityId[data['abbrID']]['id']
#     url = url_base + '/secured/ws/rest/v1/async/hospital/31313/modalityType'
#     res_add_ = requests.post(url, headers={"Authorization": "Bearer " + token,
#                                            "Content-Type": "application/json;charset=UTF-8"}, data=json.dumps(data))
#     print(res_add_.content)
#     if json.loads(res_add_.content)['header']['code'] != 200:
#         print(data)
