import requests
import os
import sys
import json
import time
import threading
from piny import YamlLoader, StrictMatcher
import mariadb
import requests
import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--file_nane", default="all", type=str,
                    help="file name call api")

parser.add_argument("-u", "--url", default="all", type=str,
                    help="url call api")

cwd = os.getcwd()  # Get the current working directory (cwd)
config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

file_name = "/study/study_success.json"
url_base = config_yml['url_base']

url_array = {'1': 'http://192.168.1.13:6789', '0': 'http://localhost:6789', '2': 'http://192.168.1.13:6789'}

args = vars(parser.parse_args())
fila_name_args = args["file_nane"]
url_arg = args["url"]
if fila_name_args != 'all':
    file_name = "/order/data_order_success" + fila_name_args + ".json"
if url_arg != 'all':
    url_base = url_array[url_arg]

print(file_name)
print(url_base)
basic = config_yml['basic']
files = str(cwd) + file_name

start_time = time.time()
if not os.path.exists(files):
    print("File path does not exist.")
    sys.exit()
if not os.path.isfile(files):
    print("File path is not a file.")
    sys.exit()

data_call_api = json.load(open(files))
index_file = len(data_call_api)
print("Reading json file time %s" % (time.time() - start_time))

accnummber_error = []
i = 0
for i_data in data_call_api:
    s1 = time.time()
    i = i + 1
    sql_update = "UPDATE hosp_study set study_time = %s  where study_iuid =%s"
    conn2 = mariadb.connect(**itechConfig)
    cur2 = conn2.cursor()
    try:
        now = datetime.datetime.strptime(i_data['studyDate'] + " " + i_data['studyTime'], '%Y-%m-%d %H:%M:%S')
        cur2.execute(sql_update, (now, now, i_data['studyInstanceUID']))
        conn2.commit()
    except:
        accnummber_error.append(data_call_api[i_data]['accessionNumber'])
        files_error = str(cwd) + "/order/data_update_order.json"
        logOrderError = open(files_error, "w+", encoding="utf-8")
        logOrderError.write(json.dumps(json.dumps(accnummber_error)))
        print("errroer")
    print(str(i))
    print(str(time.time() - s1))

    # break
