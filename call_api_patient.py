import requests
import os
import sys
import json
import time
import threading
import mariadb
from piny import YamlLoader, StrictMatcher
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

cwd = os.getcwd()  # Get the current working directory (cwd)
config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--file_nane", default="all", type=str,
                    help="file name call api")

parser.add_argument("-u", "--url", default="all", type=str,
                    help="url call api")

url_array = {'1': 'http://192.168.1.13:6789', '0': 'http://localhost:6789', '2': 'http://192.168.1.13:6789'}

url_base = config_yml['url_base_connecter']
basic = config_yml['basic_patient_report']
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

sql_accnumber = "SELECT id,pid ,initial_secret ,fullname ,gender ,birth_date,address ,email ,phone  FROM hosp_patient hp   "
conn2 = mariadb.connect(**itechConfig)
cur2 = conn2.cursor()
cur2.execute(sql_accnumber)
results = cur2.fetchall()
data_json = []
for row in results:
    data = {}
    data['id'] = row[0]
    data['pid'] = row[1]
    data['initialSecret'] = row[2]

    data['fullname'] = row[3]
    data['gender'] = row[4]
    data['birthDate'] = row[5]

    data['address'] = row[6]
    data['email'] = row[7]
    data['phone'] = row[8]
    data_json.append(data)

for json_data in data_json:
    # url = url_base + '/admin/ws/rest/v1/async/patient'
    # x = requests.post(url, data=json.dumps(json),
    #                   headers={"Authorization": "Basic " + basic,
    #                            "Content-Type": "application/json"})
    print(json.dumps(json_data))



print(len(data_json))
