import sys

import mariadb
import json
import time
import requests
import os
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import threading
from piny import YamlLoader, StrictMatcher
import datetime

cwd = os.getcwd()

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}
basic = config_yml['basic']

url_base = config_yml['url_base']

modality = {
    '1': 'CT',
    '2': 'MR',
    '11': 'DX',
    '9': 'US',
    '17': 'ES',
    '19': 'OP',
    '18': 'SC',
    '16': 'ECG',
    '10': 'ED',
    '12': 'XA'
}
conn = mariadb.connect(**config)
cur = conn.cursor()
start_time = time.time()

countAPI = 0
countGetDB = 0

start_time_time = time.time()
sqlRis = "select\n" \
         "    fm.accessionNumber,\n" \
         "    fm.groupModality,\n" \
         "    fm.diagnosis,\n" \
         "    fm.jsonData,\n" \
         "    fm.studyIUID,\n" \
         "  fm.patientFK ,fm.id\n" \
         "from ris_mwl fm where 1=1 \n" \
         " and fm.`date` >= ? and fm.`date` <= ?"
parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--start_date", default="all", type=str,
                    help="Start day to scan data if not transport will default is the first day formatDate :YYYY-mm-dd")
parser.add_argument("-e", "--end_date", default="all", type=str,
                    help="The default end date with no data is today formatDate :YYYY-mm-dd")
args = vars(parser.parse_args())

start_date = args["start_date"]
end_date = args["end_date"]

if start_date > end_date:
    print("end_date lon hon start_date")
    sys.exit()
if start_date == "all" or end_date == 'all':
    print("Nhap day du start date vs end date")
    sys.exit()


def call_api(index_start, index_end, thread_index):
    jsonError = []
    total = 0
    i_z = 0
    if index_start > index_file:
        return
    for i in range(index_start, index_end):
        url = url_base + "/conn/ws/rest/v1/hospital/31313/order"
        row = json_success[i]
        s1 = time.time()
        x = requests.post(url, data=json.dumps(row),
                          headers={"Authorization": "Basic " + basic,
                                   "Content-Type": "application/json"})
        print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
        total = total + 1
        if x.status_code != 200:
            print(row)
            i_z = i_z + 1
            jsonError.append(row)
            print("total error " + str(i_z) + "/" + str(total) + "  thread " + str(thread_index))
            files = str(cwd) + "/filelog/error_study_end_start_" + str(thread_index) + ".txt"
            print(files)
            # break
            os.makedirs(os.path.dirname(files), exist_ok=True)

            print(files)
            with open(files, 'w+') as outFile:
                outFile.write(json.dumps(jsonError))
    print("--- %s seconds2 end thread---" % (time.time() - start_time))


cur.execute(sqlRis, (start_date, end_date,))
results = cur.fetchall()
print("--- %s seconds ---" % (time.time() - start_time_time))
json_success = []
json_error = []
print(len(results))
query_patient = "select rp.id,rp.address,rp.pid,rp.birthYear,rp.name,rp.sex,rp.phone from ris_patient rp"
cur.execute(query_patient)
results_patient = cur.fetchall()
jsonMapPatient = {}
json_error_format = []
for row in results_patient:
    jsonMapPatient[str(row[0])] = row

for row in results:
    jsonData = {}
    isCallApi = 1
    jsonData['orderNumber'] = row[0]
    jsonData['modalityType'] = modality.get(row[1])
    jsonData['accessionNumber'] = row[0]
    jsonData['clinicalDiagnosis'] = row[2]
    if row[3] is not None:
        dataJson = json.loads(row[3])
        jsonData['requestedDepartmentCode'] = ''
        jsonData['referringPhysicianCode'] = ''
        dateListOrderProcedure = []
        if dataJson.get("orders") is not None and len(dataJson.get("orders")):
            orderProcedure = {'requestedNumber': row[4], 'requestedProcedureCode': dataJson['orders'][0]['code'],
                              'requestedProcedureName': dataJson['orders'][0]['name'],
                              'requestedModalityType': dataJson['orders'][0]['group'],
                              }
            if dataJson['orderDate'] is not None:
                try:
                    orderProcedure['requestedDatetime'] = datetime.datetime.strptime(dataJson['orderDate'],
                                                                                     '%Y-%m-%d %H:%M:%S')
                except:
                    print(row[0])
                    json_error_format.append(row[0])
                    files = str(cwd) + "/filelog/data_error_study_end_start_date_convent" + ".txt"
                    with open(files, 'w+') as file_write:
                        file_write.write(json.dumps(json_error_format))
                    os.makedirs(os.path.dirname(files), exist_ok=True)
            dateListOrderProcedure.append(orderProcedure)
        else:
            isCallApi = 0
        jsonData['services'] = dateListOrderProcedure
        if jsonData['services'] is None and len(jsonData['services']) == 0:
            isCallApi = 0

        try:
            jsonData['requestedDepartmentName'] = dataJson['clinical']['department']
            jsonData['referringPhysicianName'] = dataJson['clinical']['doctor']
            jsonData['instructions'] = dataJson['comment']
            jsonData['orderDatetime'] = datetime.datetime.strptime(dataJson['orderDate'],
                                                                   '%Y-%m-%d %H:%M:%S')

        except:
            print("data convert error")
            isCallApi = 0

        row_patient = jsonMapPatient[str(row[5])]
        jsonPatientDTO = {'address': row_patient[1], 'pid': row_patient[2]}
        if row_patient[3] is None:
            jsonPatientDTO['birthDate'] = None
        else:
            jsonPatientDTO['birthDate'] = str(row_patient[3]) + "0101"
        jsonPatientDTO['fullname'] = row_patient[4]
        jsonPatientDTO['gender'] = row_patient[5]
        jsonPatientDTO['Phone'] = row_patient[6]
        jsonData['patient'] = jsonPatientDTO
    else:
        isCallApi = 0

    if isCallApi == 1:

        json_success.append(jsonData)
    else:
        jsonData['id'] = row[6]
        json_error.append(jsonData)

index_file = len(json_success)

files = str(cwd) + "/filelog/data_error_study_end_start" + ".txt"
with open(files, 'w+') as file_write:
    file_write.write(json.dumps(json_error))
os.makedirs(os.path.dirname(files), exist_ok=True)
try:
    threads = []
    imdex = 0
    index_thread = 5000
    while True:
        if ((imdex + 1) * index_thread) > index_file:
            t1 = threading.Thread(target=call_api, args=(index_thread * imdex, index_file, imdex))
            threads.append(t1)
            t1.start()
            break
        t1 = threading.Thread(target=call_api,
                              args=(index_thread * imdex, index_thread + index_thread * imdex - 1, imdex))
        t1.start()
        threads.append(t1)
        imdex = imdex + 1
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
