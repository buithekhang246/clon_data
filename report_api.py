import json
import os
from builtins import print
import datetime
import mariadb
import requests
from piny import YamlLoader, StrictMatcher

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
url_call = config_yml['url_base']

url_call = "http://localhost:6787"
config = {
    'host': config_yml['databases_ris']['host'],
    'port': config_yml['databases_ris']['port'],
    'user': config_yml['databases_ris']['user'],
    'password': config_yml['databases_ris']['pass'],
    'database': config_yml['databases_ris']['database'],
}
itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

conn_his = mariadb.connect(**config)
cur_his = conn_his.cursor()

conn_hosp = mariadb.connect(**itechConfig)
cur_hosp = conn_hosp.cursor()
cwd = os.getcwd()  # Get the current working directory (cwd)
files = str(cwd) + "/log_report_error.txt"
files_call_api_report = str(cwd) + "/log_call_api_report_error.txt"
logOrderError = open(files, "w+", encoding="utf-8")
logOrderError_call_api = open(files_call_api_report, "w+", encoding="utf-8")
sql = "select id,code from hosp_procedure "

cur_hosp.execute(sql)
result_hosp_procedure = cur_hosp.fetchall()
data_procedure = {}
for row in result_hosp_procedure:
    data_procedure[row[1]] = row
print(data_procedure)

# select db ris
sql_user = 'select id,account from cores_user as cu where deleted = 0'
data_user_ris = {}
cur_his.execute(sql_user)
result_user_his = cur_his.fetchall()
for row in result_user_his:
    data_user_ris[str(row[0])] = row

data_report = []
sql_report_ris = 'select rm.accessionNumber , rd.userFK, rs.accessionNumber , rsc.`key` , CONCAT(rd.reading, ' \
                 'rd.conclude) , rd.id from ris_diagnosis rd  \n' \
                 "LEFT JOIN ris_study rs on rs.id = rd.studyFK  \n" \
                 "LEFT JOIN ris_service_code rsc on rsc.id = rd.userFK  \n" \
                 "LEFT JOIN ris_mwl rm on rm.id = rs.mwlFK  \n" \
                 "where rd.id in (select max(id) from ris_diagnosis WHERE  type = \"approve\" GROUP  by studyFK , " \
                 "serviceFK) "
cur_his.execute(sql_report_ris)
list_diagnosis = cur_his.fetchall()

token_user = {}
total = 0
error = 0
for row in list_diagnosis:
    total = total + 1
    accessionNumber = None
    if token_user.get(data_user_ris[str(row[1])][1]) is None:
        url_login = url_call + "/ws/rest/v1/hospital/31313/login"
        user_pass = {}
        user_pass['username'] = data_user_ris[str(row[1])][1]
        print(data_user_ris[str(row[1])][1])
        user_pass['password'] = '123456'
        res_token = requests.post(url_login, headers={
            "Content-Type": "application/json;charset=UTF-8"}, data=json.dumps(user_pass))
        print(res_token.content)
        respones_api_token = json.loads(res_token.content)
        if respones_api_token.get('token') is not None:
            token = json.loads(res_token.content)['token']['access_token']
            token_user[data_user_ris[str(row[1])][1]] = token
        # serviceId user
    url_getOrder = url_call + "/secured/ws/rest/v1/async/hospital/31313/search/order"

    if row[2] is not None:
        accessionNumber = row[2]
    elif row[0] is not None:
        accessionNumber = row[0]
    if accessionNumber is not None:
        data_search = {}
        data_search['accessionNumber'] = accessionNumber
        res_data_search = requests.post(url_getOrder,
                                        headers={"Authorization": "Bearer " + token_user[data_user_ris[str(row[1])][1]],
                                                 "Content-Type": "application/json;charset=UTF-8"},
                                        data=json.dumps(data_search))
        json_data = json.loads(res_data_search.content)
        serviceId = None
        if json_data.get('body') is not None and len(json_data['body']) > 0:
            if json_data['body'][0]['services'] is not None and len(json_data['body'][0]['services']) > 0:
                serviceId = json_data['body'][0]['services'][0]['id']
        if serviceId is None:
            jsonError = {'accessionNumber_mwl': row[0], 'userFK': row[1], 'accessionNumber_study': row[2],
                         'key': row[3], 'text': row[4], 'id': row[5]}

            jsonSave = json.dumps(jsonError).encode('utf-8')
            logOrderError.write(str(json.loads(jsonSave)))
            logOrderError.write("\n")
            error = error + 1
            # print("serviceId is None")
            print(str(error) + "/" + str(total))
        else:
            url = url_call + "/secured/ws/rest/v1/async/hospital/31313/service/" + str(serviceId) + "/reportApproved"
            data_order = {'serviceID': serviceId, 'bodyHTML': row[4], 'conclusionHTML': row[4]}
            date_time = datetime.datetime.now()
            data_order['approvedDatetime'] = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
            res_data_report = requests.post(url,
                                            headers={
                                                "Authorization": "Bearer " + token_user[data_user_ris[str(row[1])][1]],
                                                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                                                'accept-encoding': 'gzip, deflate'},
                                            data=data_order)
            if res_data_report.status_code != 200 and res_data_report.status_code != 201:
                logOrderError_call_api.write(json.dumps(data_order))
                logOrderError_call_api.write("\n")
                error = error + 1
                print("api errror")
                print(res_data_report.content)
                print(data_order)
                print(token_user[data_user_ris[str(row[1])][1]])
                print(str(error) + "/" + str(total))
            else:
                print("thanh cong")
                print(accessionNumber)
                print(str(error) + "/" + str(total))
            print(data_order)

    else:
        jsonError = {}
        jsonError['accessionNumber_mwl'] = row[0]
        jsonError['userFK'] = row[1]
        jsonError['accessionNumber_study'] = row[2]
        jsonError['key'] = row[3]
        jsonError['text'] = row[4]
        jsonError['id'] = row[5]
        logOrderError.write(json.dumps(jsonError))
        logOrderError.write("\n")
        error = error + 1
        print("accessionNumber null")

        print(str(error) + "/" + str(total))
